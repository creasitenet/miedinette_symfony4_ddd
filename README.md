
## A propos

Miedinette permet de commander auprès de la miedinette.
Le site a été développé avec Symfony :
[https://symfony.com/](https://symfony.com/)

## Installation

Cloner le repo 

Composer : 
```bash
composer install
```

Lancer le serveur : 
```bash
php -S 127.0.0.1:8000
```

## Encore (assets, Less)

Pour compiler: 
```bash
yarn install
yarn encore dev
```
   
ou pour une compilation en continu
```bash
yarn encore dev --watch
```

ou pour une compilation pour la production
```bash
yarn encore production
```

## Contribuer

Pull request sur le repo 


