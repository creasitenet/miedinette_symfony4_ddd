var Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

	// add js entries
	.addEntry('app', './assets/js/app.js')
	.addEntry('themejs', './assets/js/theme.js')

	// add style entries
	.addStyleEntry('theme', './assets/css/theme.scss')

	//.createSharedEntry('vendor', ['jquery', 'bootstrap'])
	//.createSharedEntry('vendor', ['jquery'])
		
	// empty the outputPath dir before each build
     .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()

    // Less
    //.enableLessLoader

	// Sass
	.enableSassLoader()

	// enable source maps during development
	.enableSourceMaps(!Encore.isProduction())

	// allow legacy applications to use $/jQuery as a global variable
	//.autoProvidejQuery()
	/*.autoProvideVariables({
		$: 'jquery',
		jQuery: 'jquery',
		'window.jQuery': 'jquery'
	})*/

    // Versionning
    .enableVersioning() //Hashed filenames
;

module.exports = Encore.getWebpackConfig();
