<?php

namespace App\UserInterface\Form;

use App\Core\Dessert\Entity\Dessert;
use App\Core\Dish\Entity\Dish;
use App\Core\Order\Entity\Order;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dish',EntityType::class, [
                'label' => 'Plat',
                'class' => Dish::class,
                'choice_label' => 'name',
                'attr' => array(
                    'class' => 'form-control form-control-select',
                )
            ])
            ->add('dish_add', TextType::class,[
                'label' => 'Ajout plat',
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false
            ])
            ->add('dessert',EntityType::class, [
                'label' => 'Dessert',
                'class' => Dessert::class,
                'choice_label' => 'name',
                'attr' => array(
                    'class' => 'form-control form-control-select',
                )
            ])
            ->add('dessert_add', TextType::class,[
                'label' => 'Ajout dessert',
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
