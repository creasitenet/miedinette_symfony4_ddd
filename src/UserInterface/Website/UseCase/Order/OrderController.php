<?php

declare(strict_types=1);

namespace App\UserInterface\Website\UseCase\Order;

use App\Core\Order\Entity\Order;
use App\Core\Order\Repository\OrderRepositoryInterface;
use App\Core\Order\UseCase\Create\CreateCommand;
use App\Core\Order\UseCase\Update\UpdateCommand;
use App\Infrastructure\Framework\LoggedInUserServiceInterface;
use App\UserInterface\Form\OrderType;
//use SimpleBus\Message\Bus\MessageBus;
use SimpleBus\SymfonyBridge\Bus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/commandes")
 */
class OrderController extends AbstractController
{

    /**
     * @var LoggedInUserServiceInterface
     */
    private $loggedInUserService;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var MessageBus
     */
    //private $messageBus;
    /**
     * @var CommandBus
     */
    private $commandBus;


    public function __construct
    (
        LoggedInUserServiceInterface $loggedInUserService,
        FormFactoryInterface $formFactory,
        OrderRepositoryInterface $orderRepository,
        //MessageBus $messageBus,
        CommandBus $commandBus
    )
    {
        // $this->messageBus = $messageBus;
        $this->loggedInUserService = $loggedInUserService;
        $this->formFactory = $formFactory;
        $this->orderRepository = $orderRepository;
        $this->commandBus = $commandBus;
    }

    /**
     * @Security("is_granted('ROLE_USER')", statusCode=404, message="No access! Get out!")
     * @Route("/", name="order_index", methods="GET")
     */
    public function index(): Response
    {
        $todayOrders = $this->orderRepository->findToday($this->getUser());

        return $this->render('@Order/index.html.twig',
            [
                //'orders' => $this->getUser()->getOrders(),
                'todayOrders' => $todayOrders
            ]
        );
    }

    /**
     * @Security("is_granted('ROLE_USER')", statusCode=404, message="No access! Get out!")
     * @Route("/historique", name="order_histo", methods="GET")
     */
    public function list(): Response
    {
        return $this->render(
            '@Order/histo.html.twig',
            [
                'orders' => $this->getUser()->getOrders(),
            ]
        );
    }

    /**
     * @Security("is_granted('ROLE_USER')", statusCode=404, message="No access! Get out!")
     * @Route("/commander", name="order_new", methods="GET|POST")
     *
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        //$order = new Order();

        $form = $this->formFactory->create(OrderType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $userId = $this->loggedInUserService->getLoggedInUserId();

            $data = $form->getData();

            $command = new CreateCommand(
                $userId, //$this->getUser()->getId(),
                $data->getDish()->getId(),
                $data->getDishAdd(),
                $data->getDessert()->getId(),
                $data->getDessertAdd()
            );

            $this->commandBus->handle($command);

            return $this->redirectToRoute('order_index');
        }

        return $this->render('@Order/new.html.twig', [
            'form' => $form->createView(),
            'orders' => $this->getUser()->getOrders(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_USER')", statusCode=404, message="No access! Get out!")
     * @Route("/{id}", name="order_show", methods="GET")
     */
    public function show(Order $order): Response
    {
        return $this->render('@Order/show.html.twig', [
            'order' => $order
        ]);
    }

    /**
     * @Security("is_granted('ROLE_USER')", statusCode=404, message="No access! Get out!")
     * @Route("/{id}/edit", name="order_edit", methods="GET|POST")
     */
    public function edit(Request $request, Order $order): Response
    {
        $form = $this->formFactory->create(OrderType::class, $order);
        //$form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            // TODO check the user right ?

            $command = new UpdateCommand(
                $data->getId(),
                $data->getDish()->getId(),
                $data->getDishAdd(),
                $data->getDessert()->getId(),
                $data->getDessertAdd()
            );

            $this->commandBus->handle($command);

            return $this->redirectToRoute('order_edit', ['id' => $order->getId()]);
        }

        return $this->render('@Order/edit.html.twig', [
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_USER')", statusCode=404, message="No access! Get out!")
     * @Route("/{id}", name="order_delete", methods="DELETE")
     */
    public function delete(Request $request, Order $order): Response
    {
        if ($this->isCsrfTokenValid('delete'.$order->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($order);
            $em->flush();
        }

        return $this->redirectToRoute('order_index');
    }

}
