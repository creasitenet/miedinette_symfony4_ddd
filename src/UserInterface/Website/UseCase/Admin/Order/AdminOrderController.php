<?php

declare(strict_types=1);

namespace App\UserInterface\Website\UseCase\Admin\Order;

use App\Core\Order\Entity\Order;
use App\Core\Order\Repository\OrderRepositoryInterface;
use App\Core\Order\UseCase\Create\CreateCommand;
use App\Infrastructure\Framework\LoggedInUserServiceInterface;
use App\UserInterface\Form\OrderType;
//use SimpleBus\Message\Bus\MessageBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/order")
 */
class AdminOrderController extends AbstractController
{
    public function __construct
    (
        LoggedInUserServiceInterface $loggedInUserService,
        FormFactoryInterface $formFactory,
        OrderRepositoryInterface $orderRepository
    )
    {
        $this->loggedInUserService = $loggedInUserService;
        $this->formFactory = $formFactory;
        $this->orderRepository = $orderRepository;
    }

	/**
     * @Security("is_granted('ROLE_ADMIN')", statusCode=404, message="No access! Get out!")
     * @Route("/admin", name="admin_order_index", methods="GET")
     */
    public function index(): Response
    {
        return $this->render('@order/index.html.twig', ['orders' => $this->orderRepository->findAll()]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')", statusCode=404, message="No access! Get out!")
     * @Route("/{id}", name="admin_order_show", methods="GET")
     */
    public function show(Order $order): Response
    {
        return $this->render('admin/order/show.html.twig', [
            'order' => $order
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')", statusCode=404, message="No access! Get out!")
     * @Route("/{id}/edit", name="admin_order_edit", methods="GET|POST")
     */
    public function edit(Request $request, Order $order): Response
    {
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_order_edit', ['id' => $order->getId()]);
        }

        return $this->render('admin/order/edit.html.twig', [
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')", statusCode=404, message="No access! Get out!")
     * @Route("/{id}", name="admin_order_delete", methods="DELETE")
     */
    public function delete(Request $request, Order $order): Response
    {
        if ($this->isCsrfTokenValid('delete'.$order->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($order);
            $em->flush();
        }

        return $this->redirectToRoute('admin_order_index');
    }

}
