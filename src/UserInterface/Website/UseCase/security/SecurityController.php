<?php

namespace App\UserInterface\Website\UseCase\Security;

use App\Core\User\Entity\User;
use App\UserInterface\Form\UserType;
use App\Core\Dish\Entity\Dish;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends Controller
{
    /**
     * @Route("/connexion", name="login")
     * @param Request $request
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {        
        // get the login error if there is one
		$error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
		$lastUsername = $authenticationUtils->getLastUsername();

		return $this->render('@Security/login.html.twig', array(
			'last_username' => $lastUsername,
            'error' => $error
		));
    }

    /**
     * @Route("/deconnexion", name="logout")
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function logout(Request $request) : RedirectResponse
    {					
		return new RedirectResponse(
            $request->headers->get('referer')
        );
    }

    /**
     * 
     * @Route("/inscription", name="register")
     * @param UserPasswordEncoderInterface $encoder
     * @return void
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();        
        $form = $this->createForm(UserType::class, $user);
		$form->handleRequest($request);
		
        if ($form->isSubmitted() && $form->isValid()) {
            
            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setRoles(['ROLE_USER']);
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('login');
        }

        return $this->render(
            '@Security/register.html.twig',
            array('form' => $form->createView())
        );
	}
}
