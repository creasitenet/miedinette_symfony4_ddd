<?php

namespace App\Domain\Service;

use App\Core\Dish\Entity\Dish;
use App\Core\Order\Entity\Order;
use App\Form\DishType;
use Doctrine\ORM\EntityManager;
use App\Core\Dish\Repository\DishRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class OrdersService
{
	private $repository;

	public function __construct(DishRepository $dishesRepository)
	{
		$this->repository = $dishesRepository;
	}

	public function insert(Request $request, EntityManager $entityManager,  TokenStorage $token_storage) : OrdersService
	{
		$entityManager->persist(
			$this->prepareOrder($request, $token_storage)
		);

		$entityManager->flush();

		return $this;
	}

	private function prepareOrder(Request $request, TokenStorage $token_storage) : Order
	{
		$order = new Order();

		foreach($request->get('order')['dishs'] as $dish){

			$order->getDishs()->add(
				$this->repository->find(
					$dish['id']
				)
			);
		}

		return $order
			->setUser($token_storage->getToken()->getUser())
			->setDate(new \DateTime(date("Y-m-d H:i:s")));
	}

}
