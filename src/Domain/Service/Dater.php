<?php

namespace App\Domain\Service;

use Carbon\Carbon;

class Dater
{
    /**
     * Gestion de l'ouverture Cloture des commandes
     * @return boolean
     */
    public function getOrderStatus() : bool
    {
        $dt = Carbon::now('Europe/Paris');
        $isOpen = false;

        // C'est pas le Weekend
        if (!Carbon::now()->isWeekend()) {
            if (($dt->hour > '07') && ($dt->hour . '' . $dt->minute < '1020')) {
                $isOpen = true;
            }
        }

        return $isOpen;
    }

    /**
     * Get date de cloture
     * @return string
     */
    public function getOrderCloseDate() : string
    {
       $dateClose = Carbon::today('Europe/Paris')->addHours(10)->addMinutes(20);
		return $dateClose;
    }

    /**
     * Get date de cloture
     * @return string
     */
    public function getOrderOpenDate() : string
    {
        $dt = Carbon::now('Europe/Paris');
        $dateOpen = Carbon::tomorrow('Europe/Paris')->addHours(07);

        // Si c'est le Weekend
        if (Carbon::now()->isWeekend()) {
            if ($dt->dayOfWeek === Carbon::FRIDAY) {
                $dateOpen = Carbon::today('Europe/Paris')->addDays(3)->addHours(07);
            }
            if ($dt->dayOfWeek === Carbon::SATURDAY) {
                $dateOpen = Carbon::today('Europe/Paris')->addDays(2)->addHours(07);
            }
        }

        return $dateOpen;
    }

}
