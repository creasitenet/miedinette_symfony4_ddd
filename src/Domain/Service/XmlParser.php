<?php

namespace App\Domain\Service;

class XmlParser
{
	private $atomFlux = 'http://lamiedinette.blogspot.com/feeds/posts/default';
	private $endOfMenu = 'plus d';

    public function parseMiedinette() : String
    {				
		$platdujour = (new \SimpleXMLElement($this->atomFlux, NULL, TRUE))->entry->content;
				
		return substr ( 
			$platdujour, 
			0, 
			stripos( 
				$platdujour,
				$this->endOfMenu
			)
		);	
    }

}
