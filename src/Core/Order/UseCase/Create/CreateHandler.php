<?php

declare(strict_types=1);

namespace App\Core\Order\UseCase\Create;

use App\Core\Dessert\Repository\DessertRepositoryInterface;
use App\Core\Dish\Repository\DishRepositoryInterface;
use App\Core\Order\Entity\Order;
use App\Core\Order\Repository\OrderRepositoryInterface;
use App\Core\User\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityManager;
use App\Core\Dessert\Repository\DessertRepository;
use App\Core\Dish\Repository\DishRepository;
use App\Core\Order\Repository\OrderRepository;
use App\Core\User\Repository\UserRepository;

class CreateHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var DessertRepositoryInterface
     */
    private $dessertRepository;

    /**
     * @var DishRepositoryInterface
     */
    private $dishRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(
        EntityManager $entityManager,
        DessertRepository $dessertRepository,
        DishRepository $dishRepository,
        OrderRepository $orderRepository,
        UserRepository $userRepository
    ) {
        $this->entityManager = $entityManager;
        $this->dessertRepository = $dessertRepository;
        $this->dishRepository = $dishRepository;
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
    }

    public function handle(CreateCommand $command): void
    {

        $user = $this->userRepository->get($command->getUserId());
        $dish = $this->dishRepository->get($command->getDishId());
        $dessert = $this->dessertRepository->get($command->getDessertId());

        $order = new Order();
        $order->setUser($user);
        $order->setDish($dish);
        $order->setDishAdd($command->getDishAdd());
        $order->setDessert($dessert);
        $order->setDessertAdd($command->getDessertAdd());
        $todayOrdersCount = $this->orderRepository->findTodayCount($command->getUserId());
        $order->setNumber($todayOrdersCount+1);
        $order->setStatus(0);

        $this->orderRepository->save($order);

    }
}
