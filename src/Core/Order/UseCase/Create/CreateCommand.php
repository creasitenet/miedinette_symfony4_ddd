<?php

declare(strict_types=1);

namespace App\Core\Order\UseCase\Create;

//use SimpleBus\Message\Name\NamedMessage;
use Symfony\Component\Validator\Constraints as Assert;

class CreateCommand
{

    /**
     * @var string
     * @Assert\NotBlank(message="Id utilisateur requis")
     */
    private $userId;

    /**
     * @var int
     * @Assert\NotBlank(message="Le plat est requis")
     */
    private $dishId;

    /**
     * @var string
     */
    private $dishAdd;

    /**
     * @var int
     * @Assert\NotBlank(message="Le dessert est requis")
     */
    private $dessertId;

    /**
     * @var string
     */
    private $dessertAdd;

    public function __construct(
        string $userId,
        int $dishId,
        ?string $dishAdd,
        int $dessertId,
        ?string $dessertAdd
    ) {
        $this->userId = $userId;
        $this->dishId = $dishId;
        $this->dishAdd = $dishAdd;
        $this->dessertId = $dessertId;
        $this->dessertAdd = $dessertAdd;
    }

    /*public static function messageName()
    {
        return 'create_order_command_handler';
    )*/

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getDishId(): int
    {
        return $this->dishId;
    }

    /**
     * @return string
     */
    public function getDishAdd(): string
    {
        return $this->dishAdd;
    }

    /**
     * @return int
     */
    public function getDessertId(): int
    {
        return $this->dessertId;
    }

    /**
     * @return string
     */
    public function getDessertAdd(): string
    {
        return $this->dessertAdd;
    }

}
