<?php

declare(strict_types=1);

namespace App\Core\Order\UseCase\Update;

//use SimpleBus\Message\Name\NamedMessage;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateCommand
{

    /**
     * @var string
     * @Assert\NotBlank(message="Id commande requis")
     */
    private $orderId;

    /**
     * @var int
     * @Assert\NotBlank(message="Le plat est requis")
     */
    private $dishId;

    /**
     * @var string
     */
    private $dishAdd;

    /**
     * @var int
     * @Assert\NotBlank(message="Le dessert est requis")
     */
    private $dessertId;

    /**
     * @var string
     */
    private $dessertAdd;

    public function __construct(
        int $orderId,
        int $dishId,
        ?string $dishAdd,
        int $dessertId,
        ?string $dessertAdd
    ) {
        $this->orderId = $orderId;
        $this->dishId = $dishId;
        $this->dishAdd = $dishAdd;
        $this->dessertId = $dessertId;
        $this->dessertAdd = $dessertAdd;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @return int
     */
    public function getDishId(): int
    {
        return $this->dishId;
    }

    /**
     * @return string
     */
    public function getDishAdd(): string
    {
        return $this->dishAdd;
    }

    /**
     * @return int
     */
    public function getDessertId(): int
    {
        return $this->dessertId;
    }

    /**
     * @return string
     */
    public function getDessertAdd(): string
    {
        return $this->dessertAdd;
    }

}
