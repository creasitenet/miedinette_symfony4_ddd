<?php

declare(strict_types=1);

namespace App\Core\Order\Entity;

use App\Core\Dessert\Entity\Dessert;
use App\Core\Dish\Entity\Dish;
use App\Core\User\Entity\User;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Core\Order\Repository\OrderRepository")
 * @ORM\Table(name="uorder")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\User\Entity\User", inversedBy="orders")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\Dish\Entity\Dish", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dish;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dish_add;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\Dessert\Entity\Dessert", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dessert;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dessert_add;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Callback appelé à chaque fois qu'on créé une commande
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     *
     * @return void
     */
    public function prePersist()
    {
        if(empty($this->date)) {
            $this->date = new \DateTime();
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDish(): ?Dish
    {
        return $this->dish;
    }

    public function setDish(?Dish $dish): self
    {
        $this->dish = $dish;

        return $this;
    }

    public function getDishAdd(): ?string
    {
        return $this->dish_add;
    }

    public function setDishAdd(?string $dish_add): self
    {
        $this->dish_add = $dish_add;

        return $this;
    }

    public function getDessert(): ?Dessert
    {
        return $this->dessert;
    }

    public function setDessert(?Dessert $dessert): self
    {
        $this->dessert = $dessert;

        return $this;
    }

    public function getDessertAdd(): ?string
    {
        return $this->dessert_add;
    }

    public function setDessertAdd(?string $dessert_add): self
    {
        $this->dessert_add = $dessert_add;

        return $this;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function setNumber($number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

}
