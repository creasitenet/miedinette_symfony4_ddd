<?php

declare(strict_types=1);

namespace App\Core\Order\Repository;

use App\Core\Order\Entity\Order;

interface OrderRepositoryInterface
{

    public function get(int $id);
    public function save(Order $order);
    public function findToday($user);
    public function findTodayCount($user);

}
