<?php

declare(strict_types=1);

namespace App\Core\Order\Repository;

use App\Core\Order\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

final class OrderRepository extends ServiceEntityRepository implements OrderRepositoryInterface
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function get(int $id): Order
    {
        return $this->_em->find(Order::class, $id);
    }

    public function save(Order $order): void
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }

    /**
     * @param $user
     * @return Order[]
     */
    public function findToday($user): array
    {
        // automatically knows to select Products
        // the "p" is an alias you'll use in the rest of the query
        $qb = $this->_em->createQueryBuilder()
            ->select('uo')
            ->from(Order::class, 'uo')
            ->andWhere('uo.status = 0')
            ->andWhere('uo.user = :user')
            ->setParameter('user', $user)
            ->andWhere('uo.date BETWEEN :dateStart AND :dateEnd')
            ->setParameter('dateStart', date('Y-m-d 00:00:00'))
            ->setParameter('dateEnd', date('Y-m-d 23:59:59'))
            ->orderBy('uo.date', 'DESC')
            ->getQuery();
        return $qb->execute();

        // to get just one result:
        // $order = $qb->setMaxResults(1)->getOneOrNullResult();
    }

    /**
     * @param $user
     * @return string
     */
    public function findTodayCount($user): string
    {
        // automatically knows to select Products
        // the "p" is an alias you'll use in the rest of the query
        $qb = $this->_em->createQueryBuilder()
            ->select('count(uo.id)')
            ->from(Order::class, 'uo')
            ->andWhere('uo.status = 0')
            ->andWhere('uo.user = :user')
            ->setParameter('user', $user)
            ->andWhere('uo.date BETWEEN :dateStart AND :dateEnd')
            ->setParameter('dateStart', date('Y-m-d 00:00:00'))
            ->setParameter('dateEnd', date('Y-m-d 23:59:59'))
            ->getQuery();

        return $qb->getSingleScalarResult();
    }

    /**
     * @param $user
     * @return Order[]
     */
    public function findHisto($user): array
    {
        // automatically knows to select Products
        // the "p" is an alias you'll use in the rest of the query
        $qb = $this->_em->createQueryBuilder()
            ->select('uo')
            ->from(Order::class, 'uo')
            ->andWhere('uo.status = 1')
            ->andWhere('uo.user = :user')
            ->setParameter('user', $user)
            ->orderBy('uo.date', 'DESC')
            ->getQuery();
        return $qb->execute();

        // to get just one result:
        // $order = $qb->setMaxResults(1)->getOneOrNullResult();
    }

}
