<?php

declare(strict_types=1);

namespace App\Core\User\Repository;

use App\Core\User\Entity\User;

interface UserRepositoryInterface
{

    public function get(string $id);
    public function save(User $user);

}
