<?php

declare(strict_types=1);

namespace App\Core\Dish\Repository;

use App\Core\Dish\Entity\Dish;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

final class DishRepository extends ServiceEntityRepository implements DishRepositoryInterface
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Dish::class);
    }

    public function get(int $id): Dish
    {
        return $this->_em->find(Dish::class, $id);
    }

    public function save(Dish $dish): void
    {
        $this->_em->persist($dish);
        $this->_em->flush();
    }

}
