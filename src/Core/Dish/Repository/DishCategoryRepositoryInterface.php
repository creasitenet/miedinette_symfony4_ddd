<?php

declare(strict_types=1);

namespace App\Core\Dish\Repository;

use App\Core\Dish\Entity\DishCategory;

interface DishCategoryRepositoryInterface
{

    public function get(int $id);
    public function save(DishCategory $dishCategory);

}
