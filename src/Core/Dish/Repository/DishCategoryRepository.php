<?php

declare(strict_types=1);

namespace App\Core\Dish\Repository;

use App\Core\Dish\Entity\DishCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

final class DishCategoryRepository extends ServiceEntityRepository implements DishCategoryRepositoryInterface
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DishCategory::class);
    }

    public function get(int $id): DishCategory
    {
        return $this->_em->find(DishCategory::class, $id);
    }

    public function save(DishCategory $dishCategory): void
    {
        $this->_em->persist($dishCategory);
        $this->_em->flush();
    }

}
