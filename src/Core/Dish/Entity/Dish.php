<?php

declare(strict_types=1);

namespace App\Core\Dish\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Core\Dish\Repository\DishRepository")
 */
class Dish
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\Dish\Entity\DishCategory", inversedBy="dishes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dishcategory;

    /**
     * @ORM\OneToMany(targetEntity="App\Core\Order\Entity\Order", mappedBy="dish", orphanRemoval=true)
     */
    private $orders;

    public function __construct()
    {
        //$this->ordersItems = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDishcategory(): ?DishCategory
    {
        return $this->dishcategory;
    }

    public function setDishcategory(?DishCategory $dishcategory): self
    {
        $this->dishcategory = $dishcategory;

        return $this;
    }

    /**
     * @return Collection|Dish[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * Generates the magic method
     *
     */
    public function __toString(){
        // to show the name of the Category in the select
        return $this->name;
        // to show the id of the Category in the select
        // return $this->id;
    }

}
