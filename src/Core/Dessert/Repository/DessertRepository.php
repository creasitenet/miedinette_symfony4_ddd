<?php

declare(strict_types=1);

namespace App\Core\Dessert\Repository;

use App\Core\Dessert\Entity\Dessert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

final class DessertRepository extends ServiceEntityRepository implements DessertRepositoryInterface
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Dessert::class);
    }

    public function get(int $id): Dessert
    {
        return $this->_em->find(Dessert::class, $id);
    }

    public function save(Dessert $dessert): void
    {
        $this->_em->persist($dessert);
        $this->_em->flush();
    }

}

