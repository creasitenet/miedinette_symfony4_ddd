<?php

declare(strict_types=1);

namespace App\Core\Dessert\Repository;

use App\Core\Dessert\Entity\Dessert;

interface DessertRepositoryInterface
{

    public function get(int $id);
    public function save(Dessert $dessert);

}
