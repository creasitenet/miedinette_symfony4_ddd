<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Exception;

use RuntimeException;

/**
 * Exception thrown if an error occurs which can only be found on runtime.
 * For example an error in configuration, that should be detected and fixed before reaching production,
 * or an http call to a 3rd party API.
 *
 * @see http://php.net/manual/en/class.runtimeexception.php
 */
abstract class AbstractRuntimeException extends RuntimeException implements ExceptionInterface
{
    use ExceptionTrait;
}
