<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Exception;

use Throwable;

/**
 * This is just a tag interface so we can have a way to catch all of the application exceptions
 */
interface ExceptionInterface extends Throwable
{
}
