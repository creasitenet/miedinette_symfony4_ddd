<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Exception;

final class DateTimeException extends AbstractRuntimeException
{
}
