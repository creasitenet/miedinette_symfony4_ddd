<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Exception;

use Throwable;

trait ExceptionTrait
{
    /**
     * @var array
     */
    private $context;

    public function __construct(string $message = '', int $code = 0, Throwable $previous = null, array $context = [])
    {
        parent::__construct($message ?: $this->extractClassShortName(static::class), $code, $previous);

        $this->context = $context;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    private function extractClassShortName(string $fullyQualifiedClassName): string
    {
        return substr($fullyQualifiedClassName, strrpos($fullyQualifiedClassName, '\\') + 1);
    }
}
