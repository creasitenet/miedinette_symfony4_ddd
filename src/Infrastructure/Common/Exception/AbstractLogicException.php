<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Exception;

use LogicException;

/**
 * This kind of exceptions should be caught and handled elegantly by the application.
 *
 * @see http://php.net/manual/en/class.logicexception.php
 */
abstract class AbstractLogicException extends LogicException implements ExceptionInterface
{
    use ExceptionTrait;
}
