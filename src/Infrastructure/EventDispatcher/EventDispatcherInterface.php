<?php

declare(strict_types=1);

namespace App\Infrastructure\EventDispatcher;

interface EventDispatcherInterface
{
    public function dispatch(EventInterface $event, array $metadata = []): void;
}
