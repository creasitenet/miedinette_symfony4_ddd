<?php

declare(strict_types=1);

namespace App\Infrastructure\EventDispatcher;

use DateTimeImmutable;

interface EventInterface
{
    public function getId(): string;

    public function getName(): string;

    public function getEventDate(): DateTimeImmutable;
}
