<?php

declare(strict_types=1);

namespace App\Infrastructure\EventDispatcher;

use Psr\Container\ContainerInterface;

final class SynchronizedEventDispatcher implements EventDispatcherInterface
{
    public const SERVICE_ID = 'service_id';
    public const METHOD = 'method';

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $listenerMapping;

    public function __construct(ContainerInterface $container, array $listenerMapping)
    {
        $this->container = $container;
        $this->listenerMapping = $listenerMapping;
    }

    public function dispatch(EventInterface $event, array $metadata = []): void
    {
        $listeners = $this->listenerMapping[get_class($event)] ?? [];

        foreach ($listeners as $listener) {
            $listener = $this->getListener($listener[self::SERVICE_ID], $listener[self::METHOD]);

            $listener($event, $metadata);
        }
    }

    private function getListener(string $serviceId, string $method): callable
    {
        $listener = $this->container->get($serviceId);

        return [$listener, $method];
    }
}
