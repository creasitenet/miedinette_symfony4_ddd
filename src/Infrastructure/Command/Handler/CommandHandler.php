<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\Handler;

use App\Infrastructure\Command\Command;

interface CommandHandler
{
    public function handle(Command $command);
}
