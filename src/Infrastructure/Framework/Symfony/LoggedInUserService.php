<?php

declare(strict_types=1);

namespace App\Infrastructure\Framework\Symfony;

use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Core\User\Entity\User;
use App\Core\User\Repository\UserRepository;
use App\Infrastructure\Framework\LoggedInUserServiceInterface;

final class LoggedInUserService implements LoggedInUserServiceInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(TokenStorageInterface $tokenStorage, UserRepository $userRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->userRepository = $userRepository;
    }

    public function isAnonymousUser(): bool
    {
        return empty($this->getUser()) || $this->getUser() == 'anon.';
    }

    public function isLoggedInUser(): bool
    {
        return $this->getUser();
    }

    /**
     * @throws \LogicException
     *
     * @return string
     */
    public function getLoggedInUserId(): string
    {
        return $this->getUser()->getId();
    }

    public function getLoggedInUser(): User
    {
        return $this->userRepository->find($this->getLoggedInUserId());
    }

    /**
     * Get a user from the Security Token Storage.
     *
     * @throws LogicException If SecurityBundle is not available
     *
     * @return mixed
     *
     * @see TokenInterface::getUser()
     */
    private function getUser()
    {
        $token = $this->tokenStorage->getToken();
        if ($token === null) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }

}
