<?php

declare(strict_types=1);

namespace App\Infrastructure\Framework;

use App\Core\User\Entity\User;

interface LoggedInUserServiceInterface
{
    public function isAnonymousUser(): bool;

    public function isLoggedInUser(): bool;

    public function getLoggedInUser(): User;

    public function getLoggedInUserId(): string;
}
