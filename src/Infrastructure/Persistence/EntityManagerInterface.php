<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence;

interface EntityManagerInterface
{
    /**
     * Persist the (Doctrine) entity, replacing it if it already exists in the database.
     *
     * @param object $entity
     * @param bool $flush
     */
    public function merge($entity, $flush = true);

    /**
     * Persist the (Doctrine) entity.
     *
     * @param object $entity
     * @param bool $flush
     */
    public function persist($entity, $flush = true);

    /**
     * Flush the entities.
     */
    public function flush();

    /**
     * Remove the entity from the database.
     *
     * @param object $entity
     * @param bool $flush
     */
    public function remove($entity, $flush = true);

    /**
     * Clear the cache.
     *
     * @return mixed
     */
    public function clear();
}
