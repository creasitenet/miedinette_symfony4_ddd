<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence;

use Doctrine\ORM\EntityManagerInterface;
use App\Infrastructure\Persistence\Exception\NonUniqueResultException;
use App\Infrastructure\Persistence\Exception\NoResultException;
use function is_array;

abstract class AbstractDoctrineRepository
{
    public const NO_RESULT_EXCEPTION_CLASS = NoResultException::class;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    final protected function returnEntityOrThrowException($queryResult)
    {
        if ($queryResult === null || (is_array($queryResult) && \count($queryResult) === 0)) {
            throw $this->createNotFoundException();
        }

        if (is_array($queryResult) && \count($queryResult) > 1) {
            throw new NonUniqueResultException();
        }

        if (is_array($queryResult) && \count($queryResult) === 1) {
            return array_pop($queryResult);
        }

        return $queryResult;
    }

    private function createNotFoundException(): NoResultException
    {
        $className = static::NO_RESULT_EXCEPTION_CLASS;

        return new $className();
    }
}
