<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Exception;

use App\Infrastructure\Common\Exception\AbstractLogicException;

class NoResultException extends AbstractLogicException
{
}
