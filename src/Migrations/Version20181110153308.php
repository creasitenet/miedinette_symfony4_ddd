<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181110153308 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE dessert (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE dish (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, dishcategory_id INTEGER NOT NULL, name VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_957D8CB86DBB75BC ON dish (dishcategory_id)');
        $this->addSql('CREATE TABLE dish_category (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE uorder (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id CHAR(36) DEFAULT NULL --(DC2Type:guid)
        , dish_id INTEGER NOT NULL, dessert_id INTEGER NOT NULL, dish_add VARCHAR(255) DEFAULT NULL, dessert_add VARCHAR(255) DEFAULT NULL, date DATETIME NOT NULL)');
        $this->addSql('CREATE INDEX IDX_D9265954A76ED395 ON uorder (user_id)');
        $this->addSql('CREATE INDEX IDX_D9265954148EB0CB ON uorder (dish_id)');
        $this->addSql('CREATE INDEX IDX_D9265954745B52FD ON uorder (dessert_id)');
        $this->addSql('CREATE TABLE user (id CHAR(36) NOT NULL --(DC2Type:guid)
        , username VARCHAR(25) NOT NULL, password VARCHAR(64) NOT NULL, email VARCHAR(191) NOT NULL, is_active BOOLEAN NOT NULL, phone VARCHAR(15) NOT NULL, roles VARCHAR(191) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        //$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE dessert');
        $this->addSql('DROP TABLE dish');
        $this->addSql('DROP TABLE dish_category');
        $this->addSql('DROP TABLE uorder');
        $this->addSql('DROP TABLE user');
    }
}
