<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181110154554 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO `user` (`id`, `username`, `password`, `email`, `is_active`, `phone`, `roles`) VALUES
	(\'2cdc1fe4-68c2-11e8-8163-6a6afd0a0866\', \'eboissel\', \'$2y$13$J5sA5ds3tvffgMfybWGeyeR3nhlQj3CKB9fLhiaNRNtzcvAKAXUl6\', \'e.boissel@travaux.com\', 1, \'0641685251\', \'["ROLE_ADMIN"]\')');
        $this->addSql('INSERT INTO `user` (`id`, `username`, `password`, `email`, `is_active`, `phone`, `roles`) VALUES
	(\'5ddaa638-68b4-11e8-8163-6a6afd0a0866\', \'edouard\', \'$2y$13$2JonUlVWlDpJ6xVDROE6ieWIcq2.oZcjMk5/k8siQ3BK5nFzzo91O\', \'edouardboissel@gmail.com\', 1, \'0641685250\', \'["ROLE_USER"]\')');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('TRUNCATE TABLE user');
    }
}
