<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181110153952 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO `dish_category` (`id`, `name`) VALUES(1, \'Plat\')');
        $this->addSql('INSERT INTO `dish_category` (`id`, `name`) VALUES(2, \'Burger\')');
        $this->addSql('INSERT INTO `dish_category` (`id`, `name`) VALUES(3, \'Salade\')');
        $this->addSql('INSERT INTO `dish_category` (`id`, `name`) VALUES(4, \'Sandwitch\')');
        $this->addSql('INSERT INTO `dish_category` (`id`, `name`) VALUES(5, \'Wok\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

        $this->addSql('TRUNCATE TABLE dish_category');
    }
}
