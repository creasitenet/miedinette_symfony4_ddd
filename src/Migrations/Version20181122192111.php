<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181122192111 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_D9265954148EB0CB');
        $this->addSql('DROP INDEX IDX_D9265954745B52FD');
        $this->addSql('DROP INDEX IDX_D9265954A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__uorder AS SELECT id, user_id, dish_id, dessert_id, dish_add, dessert_add, status, date FROM uorder');
        $this->addSql('DROP TABLE uorder');
        $this->addSql('CREATE TABLE uorder (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, dish_id INTEGER NOT NULL, dessert_id INTEGER NOT NULL, user_id CHAR(36) DEFAULT NULL --(DC2Type:guid)
        , dish_add VARCHAR(255) DEFAULT NULL COLLATE BINARY, dessert_add VARCHAR(255) DEFAULT NULL COLLATE BINARY, number INTEGER DEFAULT NULL, status INTEGER NOT NULL, date DATETIME NOT NULL, CONSTRAINT FK_D9265954A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_D9265954148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_D9265954745B52FD FOREIGN KEY (dessert_id) REFERENCES dessert (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO uorder (id, user_id, dish_id, dessert_id, dish_add, dessert_add, status, date) SELECT id, user_id, dish_id, dessert_id, dish_add, dessert_add, status, date FROM __temp__uorder');
        $this->addSql('DROP TABLE __temp__uorder');
        $this->addSql('CREATE INDEX IDX_D9265954148EB0CB ON uorder (dish_id)');
        $this->addSql('CREATE INDEX IDX_D9265954745B52FD ON uorder (dessert_id)');
        $this->addSql('CREATE INDEX IDX_D9265954A76ED395 ON uorder (user_id)');
        $this->addSql('DROP INDEX IDX_957D8CB86DBB75BC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__dish AS SELECT id, dishcategory_id, name FROM dish');
        $this->addSql('DROP TABLE dish');
        $this->addSql('CREATE TABLE dish (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, dishcategory_id INTEGER NOT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_957D8CB86DBB75BC FOREIGN KEY (dishcategory_id) REFERENCES dish_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO dish (id, dishcategory_id, name) SELECT id, dishcategory_id, name FROM __temp__dish');
        $this->addSql('DROP TABLE __temp__dish');
        $this->addSql('CREATE INDEX IDX_957D8CB86DBB75BC ON dish (dishcategory_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_957D8CB86DBB75BC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__dish AS SELECT id, dishcategory_id, name FROM dish');
        $this->addSql('DROP TABLE dish');
        $this->addSql('CREATE TABLE dish (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, dishcategory_id INTEGER NOT NULL, name VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO dish (id, dishcategory_id, name) SELECT id, dishcategory_id, name FROM __temp__dish');
        $this->addSql('DROP TABLE __temp__dish');
        $this->addSql('CREATE INDEX IDX_957D8CB86DBB75BC ON dish (dishcategory_id)');
        $this->addSql('DROP INDEX IDX_D9265954A76ED395');
        $this->addSql('DROP INDEX IDX_D9265954148EB0CB');
        $this->addSql('DROP INDEX IDX_D9265954745B52FD');
        $this->addSql('CREATE TEMPORARY TABLE __temp__uorder AS SELECT id, user_id, dish_id, dessert_id, dish_add, dessert_add, status, date FROM uorder');
        $this->addSql('DROP TABLE uorder');
        $this->addSql('CREATE TABLE uorder (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, dish_id INTEGER NOT NULL, dessert_id INTEGER NOT NULL, dish_add VARCHAR(255) DEFAULT NULL, dessert_add VARCHAR(255) DEFAULT NULL, status INTEGER NOT NULL, date DATETIME NOT NULL, user_id CHAR(36) DEFAULT NULL COLLATE BINARY)');
        $this->addSql('INSERT INTO uorder (id, user_id, dish_id, dessert_id, dish_add, dessert_add, status, date) SELECT id, user_id, dish_id, dessert_id, dish_add, dessert_add, status, date FROM __temp__uorder');
        $this->addSql('DROP TABLE __temp__uorder');
        $this->addSql('CREATE INDEX IDX_D9265954A76ED395 ON uorder (user_id)');
        $this->addSql('CREATE INDEX IDX_D9265954148EB0CB ON uorder (dish_id)');
        $this->addSql('CREATE INDEX IDX_D9265954745B52FD ON uorder (dessert_id)');
    }
}
