<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181110153520 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (1, \'Tarte du jour\')');
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (2, \'Crumble du jour\')');
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (3, \'Salade de fruit frais\')');
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (4, \'Mix de fruit\')');
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (5, \'Tarte aux fruits de saison\')');
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (6, \'Tarte au Nutella\')');
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (7, \'Moelleux au chocolat\')');
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (8, \'Cheescake Framboise\')');
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (9, \'Cheescake Oreo\')');
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (10, \'Tiramisu Speculos\')');
        $this->addSql('INSERT INTO `dessert` (`id`, `name`) VALUES (11, \'Tiramisu du jour\')');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('TRUNCATE TABLE dessert');
    }
}
