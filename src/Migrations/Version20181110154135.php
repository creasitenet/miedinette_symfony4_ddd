<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181110154135 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (1, 1, \'Plat du jour\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (2, 2, \'Burger du mois\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (3, 3, \'Salade du mois\')');

        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (21, 2, \'Burger Cheese\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (22, 2, \'Burger Sicilien\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (23, 2, \'Burger Emmental\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (24, 2, \'Burger Indien\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (25, 2, \'Burger Montagnard\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (26, 2, \'Burger Chicken\')');

        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (31, 3, \'Salade Thaï Poulet\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (32, 3, \'Salade Thaï Crevettes\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (33, 3, \'Salade Thaï Boeuf\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (34, 3, \'Salade Thaî Porc laqué\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (35, 3, \'Salade Thaï Mangue\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (36, 3, \'Salade Caesar\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (37, 3, \'Salade Niçoise\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (38, 3, \'Salade Sicilienne\')');

        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (41, 4, \'Sandwitch Steack Haché\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (42, 4, \'Sandwich Kebab\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (43, 4, \'Sandwitch Aiguillettes poulet\')');

        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (51, 5, \'Wok Thaï-Thaï Poulet\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (52, 5, \'Wok Thaï-Thaï Crevette\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (53, 5, \'Wok Boeuf\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (54, 5, \'Wok Mignon\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (55, 5, \'Wok NOeuf\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (57, 5, \'Wok Italien Jambon Aoste\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (58, 5, \'Wok Italien Poulet\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (59, 5, \'Wok Italien Steack haché\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (60, 5, \'Wok 4 fromages\')');
        $this->addSql('INSERT INTO `dish` (`id`, `dishcategory_id`, `name`) VALUES (61, 5, \'Wok Végéta\')');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('TRUNCATE TABLE dish');
    }
}
