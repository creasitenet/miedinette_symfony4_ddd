$(document).ready(function(){
	
	"use strict";
    
    /* function Slider  Title animated :Auto  */
	function loadSlidingTitleAnimated(){
		var myInterval;
		var counter = 1;
		var myFunc = function() {
			var cur = $('.sliding-title ul li').length;
			if(cur == counter) {; //if conditon for resetting counter
				$('.sliding-title ul li.sliding').removeClass('sliding');
				$('.sliding-title ul li').first().addClass('sliding');
				counter = 1;
            } else {
				counter++;
				$('.sliding-title ul li.sliding').removeClass('sliding').next().addClass('sliding');
				}
		};
		myInterval = setInterval(myFunc, 5000); // Set Animation Time Intervals in Miliseconds
	}
	
	loadSlidingTitleAnimated();

});