/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// loads the jquery package from node_modules
var $ = require('jquery');
global.$ = global.jQuery = $;

require('popper.js');

require('bootstrap/dist/js/bootstrap');

require('jquery-countdown');

// Document ready
$(document).ready(function() {
	/* function Slider  Title animated :Auto  */
	function loadSlidingTitleAnimated(){
		var myInterval;
		var counter = 1;
		var myFunc = function() {
			var cur = $('.sliding-title li').length;
			if(cur == counter) {; //if conditon for resetting counter
				$('.sliding-title li.sliding').removeClass('sliding');
				$('.sliding-title li').first().addClass('sliding');
				counter = 1;
			} else {
				counter++;
				$('.sliding-title li.sliding').removeClass('sliding').next().addClass('sliding');
			}
		};
		myInterval = setInterval(myFunc, 6000); // Set Animation Time Intervals in Miliseconds
	}

	loadSlidingTitleAnimated();
});

